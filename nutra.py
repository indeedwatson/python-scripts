#!/bin/env python3
# Dark/light theme switcher for linux.
# Usage nutra.py [dark|light]
# TODO: add dunst, rofi
import os
import sys
import subprocess

confDir = os.path.expanduser('~') + "/.config"
theme = str(sys.argv[1])
wpDir = os.path.expanduser('~') + "/Pictures/wallpapers/*"


class App:

    def __init__(self, config: str, dark: dict, light: dict):
        self.config = confDir + config
        self.dark = dark
        self.light = light

    def __iter__(self):
        return self.config, self.dark, self.light


def replace(config: str, old: dict, new: dict):
    with open(config, 'r') as conf:
        data = conf.read()
        for o, n in zip(old, new):
            if not n + "\n" in data:
                data = data.replace(o, n)

    with open(config, 'w') as conf:
        conf.write(data)


apps = {
    # Config, dark, light
    "nvim":      App("/nvim/init.vim",
                 ["colorscheme seoul256", "theme ='seoul256',"],
                 ["colorscheme seoul256-light", "theme ='onelight',"]),

    "alacritty": App("/alacritty/alacritty.yml",
                 ["colors: *dark"],
                 ["colors: *light"]),

    "polybar":   App("/polybar/config.ini", 
                 ["include-file = ~/.config/polybar/theme.ini"],
                 ["include-file = ~/.config/polybar/seoul256-light.ini"]),

    "bspwm":     App("/bspwm/bspwmrc",
                 ['bspc config focused_border_color "#DE7E9E"',
                 'bspc config normal_border_color "#373B41"'],
                 ['bspc config focused_border_color "#8ABEB7"',
                  'bspc config normal_border_color "#C5C8C6"'])
}

def main():
    for app in apps.values():
        if theme == "dark":
            old, new = app.light, app.dark
        elif theme == "light":
            old, new = app.dark, app.light
        else:
            print("Usage: \npython3 nutra.py [dark|light]")
        replace(app.config, old, new)
    changeWP = "feh --randomize --bg-fill " + wpDir
    # Change the wallpaper and relaunch necessary programs
    process = subprocess.Popen(changeWP, shell=True)
    process = subprocess.Popen("polybar.sh")
    process = subprocess.Popen("bspc wm -r", shell=True)

if __name__ == "__main__":
    main()
